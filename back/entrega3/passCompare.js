const bcrypt = require('bcrypt');

(async () => {
   
    const passwordIsvalid = await bcrypt.compare(process.argv[2], '$2b$10$BC.JWbSMToxv2wNs4OazFeilVp7BizwibKzxS4g7gysitqIpK02cG');

    console.log(passwordIsvalid)
})()
