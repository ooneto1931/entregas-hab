let userAdmin = [
    {
        email: 'admin@mitienda.es',
        password: '$2b$10$BC.JWbSMToxv2wNs4OazFeilVp7BizwibKzxS4g7gysitqIpK02cG'
    }
];

let id = 1;

const getUser = (email) => {
    const matchEmail = user => user.email === email;

    return userAdmin.find( matchEmail );
}

const saveUser = (email, password, role) => {
    userAdmin.push({
        id: id++,
        email,
        password,
        role: role
    })
}


module.exports = {
    getUser,
    saveUser
}
