const axios = require('axios');
const bodyParser = require('body-parser');
const csvtojson = require('csvtojson');
const express = require('express');
//const axiosCacheAdapter = require('axios-cache-adapter');

const app = express();



/**
 * Un cliente nos pide realizar un sistema para gestionar eventos culturales.
 * Necesita dar de alta eventos, que pueden ser de tipo 'concierto', 'teatro' o 
 * 'monólogo'. Cada uno se caracteriza por un 'nombre', 'aforo' y 'artista'.
 * Opcionalmente pueden incluir una descripción.
 * 
 * El cliente necesitará una API REST para añadir conciertos y poder obtener
 * una lista de los existentes.
 * 
 * El objetivo del ejercicio es que traduzcas estos requisitos a una descripción
 * técnica, esto es, decidir qué endpoints hacen falta, qué parámetros y cuáles 
 * son los código de error a devolver
 * 
 * Notas:
 *    - el conocimiento necesario para realizarlo es el adquirido hasta la clase del
 *      miércoles
 *    - llega con un endpoint GET y otro POST
 *    - el almacenamiento será en memoria, por tanto cuando se cierre el servidor
 *      se perderán los datos. De momento es aceptable esto.
 * 
 */


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


let collection = []
app.get('/poi', (req, res) => {

    res.json(collection)
})



app.post('/poi', (req, res) => {

    let data = {
        tipo: req.body.tipo,
        nombre: req.body.nombre,
        aforo: req.body.aforo,
        artista: req.body.artista,
        descripcion: req.body.descripcion

    }
    if (req.body.tipo !== 'teatro' && req.body.tipo !== 'concierto' && req.body.tipo !== 'monologo') {
        res.status(400).send();
        return
    }
    if (data.nombre === undefined || data.aforo === undefined || data.artista === undefined) {
        res.status(400).send();
        return

    }

    



    

    const collectionsOk = []
    let currentName = data.nombre
    for (collections of collection){

        if(collections.nombre === currentName){
            collectionsOk.push(collections.nombre)}
            else{continue}
    }
    if (collectionsOk.length>0){
        res.status(409).send();
        return
    }
    
    else {
        collection.push(data);
        res.send();
    }
    

        
    
    


    
});





    app.listen(8000)
