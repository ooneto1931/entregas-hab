const bcrypt = require('bcrypt');

(async () => {
   
    const passwordIsvalid = await bcrypt.compare(process.argv[2], '$2b$10$g7WzWrJYrIikXJhvy3IlUO1qnHKKpUejp1uvgLTxaplk2Aq2ewV3.');

    console.log(passwordIsvalid)
})()
