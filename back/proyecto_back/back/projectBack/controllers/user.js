const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const bd = require('./bd_mock');



/**const nodemailer = require("nodemailer");

// async..await is not allowed in global scope, must use a wrapper
async function sendEmail(email) {
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    let testAccount = await nodemailer.createTestAccount();

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: testAccount.user, // generated ethereal user
            pass: testAccount.pass, // generated ethereal password
        },
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: '"Fred Foo 👻" <foo@example.com>', // sender address
        to: email, // list of receivers
        subject: "Hello ✔", // Subject line
        text: "Hello world?", // plain text body
        html: "<b>Hello world?</b>", // html body
    });

    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}*/





const register = async (req, res) => {

    const { email, 
            password, 
            nombre,
            apellidos} = req.body;
    
    

    /*if (!email || !password) {
        res.status(400).send();
        return;
    } ver librería  @hapi/joy*/

    // comprobar si ya existe un existe un usuario con este email
    if (bd.getUser(email)) {
        res.status(409).send();
        return;
    }

    // encriptar la password (para no almacenarla en texto claro)
    // bcrypt.hash(<password en claro (ej.: 1234)>, <número de "rounds">)
    // rounds = 3
    // hash(hash(hash(password)))
    const passwordBcrypt = await bcrypt.hash(password, 10);

    // almacenamos (email, passwordBcrypt, nombre, apellido)
    bd.saveUser(email, passwordBcrypt, nombre, apellidos);

    res.send();



    //await sendEmail(email);





}

const login = async (req, res) => {
    const { email, password } = req.body;

    // buscar email en la bbdd
    // nos devolverá un JSON para el usuario con un ID, password, nombre y apellido
    const user = bd.getUser(email);

    if (!user) {
        res.status(404).send();
        return;
    }

    const passwordIsvalid = await bcrypt.compare(password, user.password);

    if (!passwordIsvalid) {
        res.status(401).send();
        return;
    }

    const tokenPayload = {
        id: user.id,
        role: user.role,
        email: user.email
    };

    const token = jwt.sign(tokenPayload, process.env.SECRET, {
        expiresIn: '1d'
    });

    res.json({
        token
    })
}


const updateProfiles = async (req, res) =>{
    const id = parseInt(req.params.id);
    bd.updateProfile(id, req.body.nombre, req.body.apellidos);
    res.json();

}

module.exports = {
    login,
    register,
    updateProfiles

}
