// https://jsonplaceholder.typicode.com/posts
// https://jsonplaceholder.typicode.com/posts/1

// a) Generar contador de mensajes por usario
// b) Generar una lista con la siguiente estructura:
/*[
    {
        userId: <userId>,
        posts: [
            {
                title: <title>
                body: <body>     // hay que obtenerlo de la segunda petición
            },
            {
                title: <title>
                body: <body>
            },
        ]
    }

]
*/

const axios = require('axios');

const URL_API = 'https://jsonplaceholder.typicode.com/posts'

async function retrieveInfo() {

    const responseApi = await axios.get('https://jsonplaceholder.typicode.com/posts');
    let apiData = responseApi.data;
    let counter = {}

    for (i = 0; i < apiData.length; i++) {
        if (counter[apiData[i].userId] != undefined) {
            counter[apiData[i].userId]++
        }
        else { counter[apiData[i].userId] = 1 }

    }

    return counter



}
retrieveInfo()
.then(result => { })
.catch(error => {
    console.log(' Imposible realizar la operación ')
})


// b) Generar una lista con la siguiente estructura:
/*[
    {
        userId: <userId>,
        posts: [
            {
                title: <title>
                body: <body>     // hay que obtenerlo de la segunda petición
            },
            {
                title: <title>
                body: <body>
            },
        ]
    }

]
*/





async function retrievePosts() {

    const responseApi2 = await axios.get('https://jsonplaceholder.typicode.com/posts');
    let apiData2 = responseApi2.data;
    let listaPosts = []

    listaPosts = apiData2.map(listPost => {
        const URL = `${URL_API}/${listPost.id}`
        return axios.get(URL)


    })


    const listPostsDetailed = await Promise.all(listaPosts)
    let newList = []

    for (i = 0; i < listPostsDetailed.length; i++) {
        newList.push(listPostsDetailed[i].data)
    }

    let acc = {}
   
    

    for (j = 0; j < newList.length; j++) {
        if (acc[newList[j].userId] === undefined) {
            acc[newList[j].userId] = {
                userId: newList[j].userId,
                post: [{

                    title: newList[j].title,
                    body: newList[j].body,
                    

                }
                ]
                
            };
            
        } else {
            acc[newList[j].userId].post.push({
                title: newList[j].title,
                body: newList[j].body
            });
            



        }
        
        
        



    }
    
    
    
    return acc
}
retrievePosts().then(result => {console.log(result)
    
})






/**
 * for (post of posts) {
    // descargo el detalle de post (`${URL}/${post.id}`)
    detailedPost = ...

    // comprobar si ya existe una entrada para este usuario
    if (users[detailedPost.userId] === undefined) {
        users[detailedPost.userId] = {
            userId: detailedPost.userId,
            posts: [
                {
                    title: detailedPost.title,
                    body: detailedPost.body
                }
            ]
        }
    } else {
        users[detailedPost.userId].posts.push({
            title: detailedPost.title,
            body:detailedPost.body});
    }
}

 *
 *
*/
