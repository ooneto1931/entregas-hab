//*/
 //* El objetivo del ejercicio es crear un nuevo array que contenga
 //* todos los hashtags del array `tweets`, pero sin repetir
 //* 
 //* Nota: como mucho hay 2 hashtag en cada tweet


const tweets = [
    'aprendiendo #javascript en  Vigo #codinglive', 
    'empezando el segundo módulo del bootcamp #ahora!',
    'hack a boss bootcamp vigo #javascript #codinglive']
let globalHashtags= [];

    function gatHashTagFromTweet(tweet){
        let hashtags=[];
        DEFAULT_HASHTAG_LENGTH = 10;

        positionFirstHashtag = tweet.indexOf('#');
        positionSecondHashTag = tweet.indexOf('#', positionFirstHashtag + 1); // busco la segunda almohadilla a partir de la primera
        
        if (positionFirstHashtag != -1) {
            // extraigo el hashtag a partir de la posición de la almohadilla y le sumo 10, longitud 
            // del hashtag según el enunciado, más 1, que es la longitud de la almohadilla
            firstHashTag = tweet.slice(positionFirstHashtag, positionFirstHashtag + DEFAULT_HASHTAG_LENGTH + 1);
            console.log(firstHashTag)
        }
        
        if (positionSecondHashTag != -1) {
            secondHashTag = tweet.slice(positionSecondHashTag, positionSecondHashTag + DEFAULT_HASHTAG_LENGTH + 1);
            console.log(secondHashTag)
        
        }
        


        return hashtags;
    }

     for(let tweety of tweets){
         let hashtags = gatHashTagFromTweet(tweety)

         for (let hashtag of hashtags){
             if (globalHashtags.indexOf(hashtag) == -1){
                 globalHashtags.push(hashtag)
             }
         }
     }


